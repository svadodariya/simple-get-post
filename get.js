var request = require('request');
const queryString = require('query-string');
var params = {
	"a": 0,
	"m": 0,
	"shopping": JSON.stringify([
		{
			"name": "string",
			"address": "India"
		}
	])
};
const stringified = queryString.stringify(params);
console.log(stringified);
request.get({
	uri: "http://httpbin.org/get?"+stringified,
	json: true,
	headers: {
        'Content-Type': 'application/json'
    }
}, function (err, resObj, body) {
	if (err) {
		console.log(err);
	} else {
		console.log(body.args);
		console.log("Then we have to parse the Array/Object");
		console.log(JSON.parse(body.args.shopping));
	}
});